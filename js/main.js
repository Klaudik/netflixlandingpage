document.addEventListener('DOMContentLoaded', function(){
    let scene = document.querySelector('.stars');
    function stars(){
        let count = 500;
        
        let i = 0;
        while(i<count){
            let star = document.createElement('div');
            star.classList.add('star');
            let x = Math.floor(Math.random() * window.innerWidth);
            let y = Math.floor(Math.random() * window.innerHeight);
            console.log();
            let duration = Math.random() * 10;
            let size = Math.random();

            star.style.setProperty("left", x + "px");
            star.style.setProperty("top", y + "px");
            star.style.setProperty("width", 1 + size + "px");
            star.style.setProperty("height", 1 + size + "px");
            star.style.setProperty("animation-duration",5 + duration + "s");
            star.style.setProperty("animation-delay",duration + "s");
            scene.appendChild(star);
            i++;
        }
    }
    stars();
});